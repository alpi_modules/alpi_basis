var filters = require('alpi_filters');

module.exports = {
    /**
     * @summary Execute a shell command.
     * @param {String} command
     * @param {(undefined|Function)} callback 
     */
    ShellCommand: function (command, callback) {
        require('child_process').execFile('sh', ['-c', command], (error, stdout, stderr) => {
            callback({
                error: error,
                stdout: stdout,
                stderr: stderr
            });
        });
    },
    /**
     * @summary Check if a command exists.
     * @param {String} package
     * @param {Function} callback 
     */
    CommandExists: function (command, callback) {
        command = filters.KeepOnly.LettersAndHyphens(command);
        module.exports.ShellCommand("whereis -b " + command + " | cut -d':' -f2", function (exists) {
            callback(exists.stdout.trim() !== "");
        });
    },
    /**
     * @summary Check which package manager is installed.
     * @param {Function} callback
     */
    PackageManager: function (callback) {
        module.exports.CommandExists("pacman", function (exists) {
            if (exists === true) {
                callback("pacman");
            } else {
                module.exports.CommandExists("apt", function (exists) {
                    if (exists === true) {
                        callback("apt");
                    } else {
                        callback("unknown");
                    }
                });
            }
        });
    },
    /**
     * @summary Check if a package is installed.
     * @param {String} package 
     * @param {Function} callback 
     */
    IsPackageInstalled: function (package, callback) {
        package = filters.KeepOnly.LettersAndHyphens(package);
        module.exports.PackageManager((packageManager) => {
            if (packageManager === "pacman") {
                module.exports.ShellCommand("pacman -Qs " + package, (exists) => {
                    callback(exists.error === null);
                });
            } else if (packageManager === "apt") {
                module.exports.ShellCommand("dpkg -s " + package, (exists) => {
                    callback(exists.error === null);
                });
            } else {
                callback(false);
            }
        });
    },
    InstallPackage: function (package, callback) {
		package = filters.KeepOnly.LettersAndHyphens(package);
		module.exports.IsPackageInstalled(package, (installed)=>{
			if(installed === false){
				module.exports.PackageManager((packageManager) => {
					if (packageManager === "pacman") {
						module.exports.ShellCommand("sudo pacman -S " + package + " --noconfirm", (e) => {
							callback(e.error === null);
						});
					} else if (packageManager === "apt") {
						module.exports.ShellCommand("sudo apt install " + package + " -y", (e) => {
							callback(e.error === null);
						});
					} else {
						callback(false);
					}
				});
			}else{
				callback(true);
			}
		});
    },
    /**
     * @summary List upgradable packages.
     * @param {Function} callback 
     */
    ListUpgradablePackages: function (callback) {
        module.exports.PackageManager((packageManager) => {
            if (packageManager === "pacman") {
                module.exports.ShellCommand("pacman -Qu", (e) => {
                    if (e.error === null) {
                        var informations = e.stdout.trim().split("\n");
                        var packages = {};
                        informations.forEach(line => {
                            line = line.split(" ");
                            packages[line[0]] = {
                                name: line[0],
                                currentVersion: line[1],
                                lastVersion: line[3]
                            };
                        });
                        callback(packages);
                    } else {
                        callback(false);
                    }
                });
            } else if (packageManager === "apt") {
                module.exports.ShellCommand("apt list --upgradable | sed -n '1!p' | tr -d']'", (e) => {
                    if (e.error === null) {
                        var informations = e.stdout.trim().split('\n');
                        var packages = {};
                        informations.forEach(line => {
                            line = line.split('/');
                            line[1] = line[1].split(' ');
                            packages[line[0]] = {
                                name: line[0],
                                currentVersion: line[1][5],
                                lastVersion: line[1][2]
                            };
                        });
                        callback(packages);
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Upgrade all upgradable packages.
     * @param {Function} callback 
     */
    UpgradePackages: function (callback) {
        module.exports.PackageManager((packageManager) => {
            if (packageManager === "pacman") {
                module.exports.ShellCommand("sudo pacman --noconfirm --overwrite", (e) => {
                    callback(e.error === null);
                });
            } else if (packageManager === "apt") {
                module.exports.ShellCommand("apt upgrade -y", (e) => {
                    callback(e.error === null);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Get the os name.
     * @param {Function} callback
     */
    OsName: function (callback) {
        module.exports.ShellCommand("lsb_release -ds", (name) => {
            callback(name.stderr === "" ? name.stdout.trim() : false);
        });
    },
    /**
     * @summary Get the os version.
     * @param {Function} callback
     */
    OsVersion: function (callback) {
        module.exports.ShellCommand("lsb_release -rs", (version) => {
            callback(version.stderr === "" ? version.stdout.trim() : false);
        });
    },
    /**
     * @summary Get the kernel release.
     * @param {Function} callback
     */
    KernelRelease: function (callback) {
        module.exports.ShellCommand("uname -r", (release) => {
            callback(release.stderr === "" ? release.stdout.trim() : false);
        });
    },
    /**
     * @summary Get the kernel version.
     * @param {Function} callback
     */
    KernelVersion: function (callback) {
        module.exports.ShellCommand("uname -v", (version) => {
            callback(version.stderr === "" ? version.stdout.trim() : false);
        });
    },
    /**
     * @summary Get the hostname.
     * @param {Function} callback
     */
    Hostname: function (callback) {
        module.exports.ShellCommand("hostname", (hostname) => {
            callback(hostname.stderr === "" ? hostname.stdout.trim() : false);
        });
    },
    /**
     * @summary Get the user who started the node server.
     * @param {Function} callback
     */
    Whoami: function (callback) {
        module.exports.ShellCommand("whoami", (user) => {
            callback(user.stderr === "" ? user.stdout.trim() : false);
        });
    },
    /**
     * @summary List connected users.
     * @param {Function} callback
     */
    ConnectedUsers: function (callback) {
        module.exports.ShellCommand("who | tr -s ' '", (infos) => {
            if (infos.stderr === "") {
                infos = infos.stdout.trim().split("\n");
                var result = [];
                infos.foreach(line => {
                    line = line.trim().split(" ");
                    result[line[0]] = line;
                });
                callback(result);
            } else {
                callback(false);
            }
        });
    },
    Shutdown: function (callback) {
        module.exports.IsPackageInstalled("systemd", (installed) => {
            if (installed === true) {
                module.exports.ShellCommand("systemctl poweroff", (e) => {
                    callback(e.error === null);
                });
            } else {
                module.exports.ShellCommand("shutdown now", (e) => {
                    callback(e.error === null);
                });
            }
        });
    },
    Reboot: function (callback) {
        module.exports.IsPackageInstalled("systemd", (installed) => {
            if (installed === true) {
                module.exports.ShellCommand("systemctl reboot", (e) => {
                    callback(e.error === null);
                });
            } else {
                module.exports.ShellCommand("reboot", (e) => {
                    callback(e.error === null);
                });
            }
        });
    },
    SuspendToRam: function (callback) {
        module.exports.IsPackageInstalled("systemd", (installed) => {
            if (installed === true) {
                module.exports.ShellCommand("systemctl suspend", (e) => {
                    callback(e.error === null);
                });
            } else {
                callback(false);
            }
        });
    },
    SuspendToDisk: function (callback) {
        module.exports.IsPackageInstalled("systemd", (installed) => {
            if (installed === true) {
                module.exports.ShellCommand("systemctl hibernate", (e) => {
                    callback(e.error === null);
                });
            } else {
                callback(false);
            }
        });
    },
    SuspendToBoth: function (callback) {
        module.exports.IsPackageInstalled("systemd", (installed) => {
            if (installed === true) {
                module.exports.ShellCommand("systemctl hybrid-sleep", (e) => {
                    callback(e.error === null);
                });
            } else {
                callback(false);
            }
        });
    }
};