var fs = require('fs');
var ejs = require('ejs');

module.exports = {
    /**
     * Render an ejs file.
     * 
     * @param {String} path
     * @param {Object} vars {varname1: value1, varname2, value2}
     */
    RenderPage: function (path, vars) {
        return ejs.render(fs.readFileSync(path, 'utf-8'), vars);
    }
};