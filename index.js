module.exports = {
    System: require('./system'),
    Date: require('./date'),
    Logger: require('./logger'),
    EjsRender: require('./ejs'),
    Hash: require('./hash'),
    Other: require('./other')
};