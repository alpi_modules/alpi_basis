var crypto = require('crypto');

module.exports = {
    /**
     * Hash the data with SHA256.
     * 
     * @param {any} data
     */
    Sha256: function (data) {
        return crypto.createHash('sha256').update(data).digest('hex');
    }
};