module.exports = {
    /**
     * SQL datetime to Javascript date
     * 
     * @param {String} SQLDate
     * 
     * @returns {Date}
     */
    DateFromSQL: function (SQLDate) {
        var date = SQLDate.split(" ")[0].split("-");
        var time = SQLDate.split(" ")[1].split(":");
        return new Date(parseInt(date[0]), parseInt(date[1]), parseInt(date[2]), parseInt(time[0]), parseInt(time[1]), parseInt(time[2]));
    },

    /**
     * Date to SQL datetime
     * 
     * @param {Date} date
     * 
     * @returns {String} 
     */
    DateToSQL: function (date) {
		var month = (((date.getMonth()+1) < 10) ? "0":"") + (date.getMonth()+1);
		var day = ((date.getDate() < 10) ? "0":"") + date.getDate();
		var hours = ((date.getHours() < 10) ? "0":"") + date.getHours();
		var minutes = ((date.getMinutes() < 10) ? "0":"") + date.getMinutes();
		var seconds = ((date.getSeconds() < 10) ? "0":"") + date.getSeconds();
        return date.getFullYear() + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
    }
};