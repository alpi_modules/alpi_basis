var path = require('path');
var appRoot = path.dirname(require.main.filename);
var fs = require('fs');

module.exports = {
    /**
     * Create an Info log.
     * 
     * @param {String} logName
     * @param {String} exception
     * @param {(null|Function)} callback
     */
    Info: function (logName, exception, callback) {
        LogFile("Info/" + logName + ".log", exception, callback);
    },
    /**
     * Create an Warning log.
     * 
     * @param {String} logName
     * @param {String} exception
     * @param {(null|Function)} callback
     */
    Warning: function (logName, exception, callback) {
        LogFile("Warning/" + logName + ".log", exception, callback);
    },
    /**
     * Create an Error log.
     * 
     * @param {String} logName
     * @param {String} exception
     * @param {(null|Function)} callback
     */
    Error: function (logName, exception, callback) {
        LogFile("Error/" + logName + ".log", exception, callback);
    },
    /**
     * Create an Other log.
     * 
     * @param {String} logName
     * @param {String} exception
     * @param {(null|Function)} callback
     */
    Other: function (logName, exception, callback) {
        LogFile(logName + ".log", exception, callback);
    }
};

/**
 * Create a log file in a log folder at the application root.
 * 
 * @param {String} logPath 
 * @param {String} exception 
 * @param {(null|Function)} callback 
 */
function LogFile(logPath, exception, callback) {
    var fullLogPath = appRoot + "/log/" + logPath;
    fs.appendFile(fullLogPath, module.exports.dateTimeNow() + ":\n" + exception + "\n\n", function (err) {
        if (err) {
            console.error("An error occurred during error reporting:\n" + err + "\n\n\nThe error was:\n" + exception);
        } else {
            var error = "Error reported to:\n" + fullLogPath + ":\n" + exception;
            var type = logPath.split("/")[0];
            if (type === "Info") {
                console.info(error);
            } else if (type === "Warning") {
                console.warn(error);
            } else if (type === "Error") {
                console.error(error);
            } else {
                console.log(error);
            }
        }
        if (callback !== undefined) {
            callback(err);
        }
    });
}