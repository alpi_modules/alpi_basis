var system = require("./system");
var filters = require('alpi_filters');

module.exports = {
    /**
     * Create a random string with multiple parameters.
     * @param {Number} size
     * @param {Boolean} numbers
     * @param {Boolean} capitalize
     * @param {Boolean} symbols
     * @param {Function} callback
     */
    RandomString: function (size, numbers, capitalize, symbols, callback) {
        var command = "pwgen " + filters.Remove.Numbers(String(size)) + " 1 ";
        command += numbers ? "-0" : "-n";
        command += capitalize ? "c" : "A";
        command += symbols ? "y" : "";
        system.ShellCommand(command, (result) => {
            callback(result.stderr === "" ? result.stdout.trim() : false);
        });
    }
};